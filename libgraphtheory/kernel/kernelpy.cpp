#include "kernelpy.h"

#include "documentwrapper.h"
#include "nodewrapper.h"
#include "edgewrapper.h"
#include "logging_p.h"
#include "kernel/modules/console/consolemodule.h"
#include "libgraphtheory/graphdocument.h"
#include <KLocalizedString>
#include <pybind11/embed.h>
#include <pybind11/stl.h>
#include <pybind11/pybind11.h>
#include <iostream>

using namespace GraphTheory;

class GraphTheory::NodeWrapperPy
{
public:
    NodePtr nodePtr;
    GraphDocumentPtr *documentWrapper;
    NodeWrapperPy(NodePtr node, GraphDocumentPtr *documentWrapperPy)
    {
        nodePtr = node;
        documentWrapper = documentWrapperPy;
    }

    ~NodeWrapperPy()
    {
    }

    int id()
    {
        return nodePtr->id();
    }
};

class GraphTheory::EdgeWrapperPy
{
public:
    EdgePtr edgePtr;
    GraphDocumentPtr *documentWrapper;
    EdgeWrapperPy(EdgePtr edge, GraphDocumentPtr *documentWrapperPy)
    {
        edgePtr = edge;
        documentWrapper = documentWrapperPy;
    }

    ~EdgeWrapperPy()
    {
    }

    NodeWrapperPy to()
    {
        return NodeWrapperPy(edgePtr->to(), documentWrapper);
    }

    NodeWrapperPy from()
    {
        return NodeWrapperPy(edgePtr->from(), documentWrapper);
    }
};

class GraphTheory::DocumentWrapperPy
{
public:
    GraphDocumentPtr pyDocument;

    DocumentWrapperPy(GraphDocumentPtr document)
    {
        pyDocument = document;
    }

    ~DocumentWrapperPy()
    {
    }

    uint getObjects()
    {
        return pyDocument->objects();
    }

    std::string documentName()
    {
        return pyDocument->documentName().toStdString();
    }

    std::vector<EdgePtr> edges()
    {
        return pyDocument->edges().toStdVector();
    }

    EdgeWrapperPy edge(int i)
    {
        return EdgeWrapperPy(pyDocument->edges()[i].data()->self(), &pyDocument);
    }

    std::vector<NodePtr> nodes()
    {
        return pyDocument->nodes().toStdVector();
    }

    GraphDocumentPtr getDocumentPtr()
    {
        return pyDocument;
    }
};

PYBIND11_EMBEDDED_MODULE(wrappers, m)
{
    pybind11::class_<GraphDocumentPtr>(m, "GraphDocumentPtr");
    pybind11::class_<GraphTheory::EdgePtr>(m, "EdgePtr");
    pybind11::class_<GraphTheory::Node>(m, "Node");
    pybind11::class_<GraphTheory::NodePtr>(m, "NodePtr");
    //.def("color", &GraphTheory::NodePtr::data().color);
    pybind11::class_<GraphTheory::EdgeList>(m, "EdgeList");
    //    .def("value", &GraphTheory::EdgeList::value);

    pybind11::class_<GraphTheory::DocumentWrapperPy>(m, "GraphDocumentWrapper")
        .def(pybind11::init<GraphDocumentPtr>())
        .def("graphDocumentPtr", &GraphTheory::DocumentWrapperPy::getDocumentPtr)
        .def("objects", &GraphTheory::DocumentWrapperPy::getObjects)
        .def("edges", &GraphTheory::DocumentWrapperPy::edges)
        .def("edge", &GraphTheory::DocumentWrapperPy::edge)
        .def("nodes", &GraphTheory::DocumentWrapperPy::nodes)
        .def("documentName", &GraphTheory::DocumentWrapperPy::documentName);

    pybind11::class_<GraphTheory::EdgeWrapperPy>(m, "EdgeWrapper")
        .def("to", &GraphTheory::EdgeWrapperPy::to)
        .def("_from", &GraphTheory::EdgeWrapperPy::from);

    pybind11::class_<GraphTheory::NodeWrapperPy>(m, "NodeWrapper")
        .def("id", &GraphTheory::NodeWrapperPy::id);
}

class PyBindOutput
{
    pybind11::object _stdout;
    pybind11::object _stderr;
    pybind11::object _stdout_buffer;
    pybind11::object _stderr_buffer;

public:
    PyBindOutput()
    {
        auto sysm = pybind11::module::import("sys");
        _stdout = sysm.attr("stdout");
        _stderr = sysm.attr("stderr");
        auto stringio = pybind11::module::import("io").attr("StringIO");
        _stdout_buffer = stringio();
        _stderr_buffer = stringio();
        sysm.attr("stdout") = _stdout_buffer;
        sysm.attr("stderr") = _stderr_buffer;
    }
    std::string getStdOut()
    {
        _stdout_buffer.attr("seek")(0);
        return pybind11::str(_stdout_buffer.attr("read")());
    }

    std::string getStdErr()
    {
        _stderr_buffer.attr("seek")(0);
        return pybind11::str(_stderr_buffer.attr("read")());
    }

    ~PyBindOutput()
    {
        auto sysm = pybind11::module::import("sys");
        sysm.attr("stdout") = _stdout;
        sysm.attr("stderr") = _stderr;
    }
};

// This function replaces reserved words to the word mappaed in the wrappers
void mapReservedWords(std::string &script, const std::string &reserved_word,
                      const std::string &map_to)
{
    size_t pointer_position = 0;
    while ((pointer_position = script.find(reserved_word, pointer_position)) != std::string::npos)
    {
        script.replace(pointer_position, reserved_word.length(), map_to);
        pointer_position += map_to.length();
    }
}

void runScript(GraphDocumentPtr document, const QString &script, auto locals)
{

    auto scriptString = script.toStdString();

    mapReservedWords(scriptString, ".from()", "._from()");

    PyBindOutput script_output{};

    pybind11::exec("Document = locals()['doc']\n" + scriptString, pybind11::globals(), locals);

    std::cout << "script output => " << std::endl
              << script_output.getStdOut() << std::endl;
}

/// BEGIN: Kernel
KernelPy::KernelPy()
{
}

KernelPy::~KernelPy()
{
}

void KernelPy::execute(GraphDocumentPtr document, const QString &script)
{
    using namespace pybind11::literals;

    pybind11::scoped_interpreter guard{};
    pybind11::module_::import("wrappers");

    GraphTheory::DocumentWrapperPy docWrapper = GraphTheory::DocumentWrapperPy(document);
    auto pyBind_locals = pybind11::dict("doc"_a = docWrapper);

    try
    {

        runScript(document, script, pyBind_locals);
    }
    catch (const std::exception &e)
    {
        std::cout << "Exception =>" << e.what() << std::endl;
    }
}

void KernelPy::stop()
{
    // TO-DO
}

void KernelPy::processMessage(const QString &messageString, KernelPy::MessageType type)
{
    // TO-DO
}

void KernelPy::attachDebugger()
{
    // TO-DO
}

void KernelPy::detachDebugger()
{
    // TO-DO
}

void KernelPy::triggerInterruptAction()
{
    // TO-DO
}

// END: Kernel
