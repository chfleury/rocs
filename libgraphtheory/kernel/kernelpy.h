#ifndef KERNELPY_H
#define KERNELPY_H

#include "graphtheory_export.h"
#include "typenames.h"
#include "node.h"
#include "graphdocument.h"

#include <QObject>
#include <QAction>


namespace GraphTheory
{
class DocumentWrapperPy;
class EdgeWrapperPy;
class NodeWrapperPy;

/**
 * \class Kernel
 */
class GRAPHTHEORY_EXPORT KernelPy : public QObject
{
    Q_OBJECT

public:
    enum MessageType {
        InfoMessage,
        WarningMessage,
        ErrorMessage
    };

    KernelPy();

    ~KernelPy() override;

    /**
     * execute python @p script on @p document
     */
    void execute(GraphTheory::GraphDocumentPtr document, const QString &script);
    void stop();

    void attachDebugger();
    void detachDebugger();
    void triggerInterruptAction();

private Q_SLOTS:
    /** process all incoming messages and resend them afterwards**/
    void processMessage(const QString &message, GraphTheory::KernelPy::MessageType type);

Q_SIGNALS:
    void message(const QString &message, GraphTheory::KernelPy::MessageType type);
    void executionFinished();
};
}

#endif
